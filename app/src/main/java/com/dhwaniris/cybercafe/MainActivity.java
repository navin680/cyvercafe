package com.dhwaniris.cybercafe;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    LinearLayout start;
    LinearLayout rating;
    private boolean nEWForrm=false;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String back = "back";
    SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (LinearLayout) findViewById(R.id.btnStart);
        rating = (LinearLayout) findViewById(R.id.rating);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Web_ViewActivity.class));
            }
        });

    }
}
